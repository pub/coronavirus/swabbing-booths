# Swabbing Booths


<img src="images/testingexterior.jpg" width="500"><br>

**Semi-permanent coronavirus testing stations designed & installed in the testing trailer behind E15 for MIT Medical**

**Please download copy of Images folder for Solidworks PDFs, our shopping list spreadsheet, & photos of mockups/details**

**Dropbox link to Solidworks file to be added here**

<p float="left">
  <img src="/trailerinterior.jpg" width="600" />
  <img src="/testingpartitions.jpg" width="600" /> 
</p>
<br>

**Issues:**

MIT must fully test & trace all on-campus community, including all asymptomatic, in order to be able to reopen campus

Sufficient virus testing for the MIT community would require massive amounts of disposable PPE

Testers don/doff PPE as infrequently as possible to reduce waste and deal with PPE shortages - meaning that with disposable PPE 
being the only barrier between testers and testees, unfortunately, food/bathroom/comfort breaks must be minimized during testers' shifts

Safe testing space was too limited; outdoor tents were attempted; are problematic for multiple reasons

**Request**

MIT Medical leased a trailer to increase testing capacity, and reached out to Prof. Marty Culpepper for help with design/fabrication of semi-permanent booths to install inside, that could replace most required wearable PPE

**teams**

MIT Medical, Facilities, IST, MIT Carpenters, and Jen OBrien & Tasker Smith took on different aspects of trailer setup 
(this page will focus on just the booths, designed & built by Jen and Tasker)

**Location:**

interior of the larger leased trailer in courtyard between MIT Medical and E15
 
**Requirements:**

-provide a semi-permanent, reusable solid barrier between testers and testees with functional installed gloves and any other required accessories

-all materials compatible with chemical sanitizers / easy to wipe down when necessary

-all ventilation directed to interior on 'clean side', creating constant positive air pressure on tester side of barrier

-seal off majority of interior, directing exhaust through vent openings at floor

-install additional duct outside booths to vent exhaust to outside, above roof of trailer

-arrange space to maximize testing capacity while keeping testers and testees at safe social distances

-minimize all skin contact with building and furniture on testee side

-ADA compliance not necessary, as ADA testing is being done elsewhere on campus


**additional goals:**

-construct with adaptable/reusable material as much as possible, to reduce waste; additional booths may also need to be set up in other locations on campus

-booths also need to be adaptable for other types of testing; design interface for easy adjustment and/or switching out of openings and accessories 

-make mounted glove locations adjustable in height, to minimize testers' discomfort and reduce potential issues with reach

-install foot pedal-activated lights at each booth to signal tester availability

-install customized shelving to hold test tubes and other equipment, assist with tube handling by testee 

-other fixtures, as needed, to be as easy to install as possible

-trailer doors are kept open during use - additional air conditioning units installed on 'clean side' to help regulate temp and humidity

**ongoing issues**

-humidity causes clear plastic panels to fog up (HVAC must maintain cool/dry interior in summer despite doors being kept open), yet trailer 
doors must be kept open to avoid cross contamination
-self-closing doors were rejected because at full capacity, they would be constantly opening and defeat the purpose

-clear panels overlap so that glove height is adjustable; panel is frequently sprayed and wiped down with sanitizer, and overlapping 
interiors are difficult to clean (design of a paddle-like tool to aid with cleaning is WIP)

-installed gloves are unwieldy when a poor fit; as they do not get removed regularly, we installed a range of sizes in the 6 booths to 
compensate. Testers wear nitrile gloves inside these (butyl rubber) arm-length gloves

(interior and exterior of booths are sanitized daily by staff)

Testing of suitable models of 2-way speaker/microphone are ongoing (conversation through barrier is difficult)

-publicly available website to be set up for sharing booth plans



**construction materials**

We went with 8020, a versatile aluminum extrusion system that has a wide range of options for fasteners, etc., and can be broken down and reused repeatedly. 
For panelling we chose .25" thick opaque and clear polycarbonate. Supply of clear PC has been extremely limited, so we used opaque sheets wherever reasonable.

MIT Carpenters constructed 2 end walls and doors out of standard interior construction materials (drywall, dimensional lumber, etc), so we were able to reserve these materials for the booth faces and partitions.